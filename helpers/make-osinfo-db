#!/bin/sh
#
#    Copyright (C) 2022 Trisquel GNU/Linux developers
#                       <trisquel-devel@listas.trisquel.info>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=2

EXTERNAL='deb-src http://ftp.us.debian.org/debian bookworm main'
REPOKEY=04EE7237B7D453EC

. ./config

rd_domain=($(cat $DATA/recommended-distros|grep -v \#|awk '{print$3}'))
rd_lcname=($(cat $DATA/recommended-distros|grep -v \#|awk '{print$5}'))

# Apply latest patchs
for patch in $(ls -v ${DATA}/*.patch)
do
    echo "Applying $patch"
    patch --no-backup-if-mismatch -Np1 < $patch
done

# Remove non-free distros references.
TMPDIR=$(mktemp -d os-XXXX)
for i in "${rd_domain[@]}"
do
    mv data/os/$i $TMPDIR || true
done
for a in $(ls data/os/)
do
    # Remove os references
    for b in $(grep -rl $a debian/patches/|sed 's|debian/patches/||')
    do
        echo "Check for $b ..."
        sed -i "/$b/d" debian/patches/series
        rm debian/patches/$b
    done
done
rm -rf data/os
mv $TMPDIR data/os

# Remove tests for non-free distros.
TMPDIR2=$(mktemp -d os-XXXX)
TMPDIR3=$(mktemp -d os-XXXX)
for i in "${rd_lcname[@]}"
do
    mv tests/isodata/$i $TMPDIR2 || true
    mv tests/treeinfodata/$i $TMPDIR3 || true
    # Remove references to non-free containers
    for a in $(ls tests/isodata/)
    do
        rm $(grep -rl $a ci/containers/) || true
        rm $(grep -rl $a ci/buildenv/) || true
    done
done
rm -rf tests/isodata
rm -rf tests/treeinfodata
mv $TMPDIR2 tests/isodata
mv $TMPDIR3 tests/treeinfodata

# Remove install-scripts
TMPDIR4=$(mktemp -d os-XXXX)
for i in "${rd_domain[@]}"
do
    mv data/install-script/$i $TMPDIR4 || true
done
rm -rf data/install-script
mv $TMPDIR4 data/install-script

# Avoid test error out.
rm tests/test_treeinfo.py \
   tests/test_related.py

#Apply changes for new fully free distros availbale at libosinfo.
# trisquel.info
sed -i '/derives-from/d' data/os/trisquel.info/trisquel-9.xml.in
sed -i '/release-date/a \ \ \ \ <codename>Etiona</codename>' data/os/trisquel.info/trisquel-9.xml.in

changelog "Remove non-free suggestions from db osinfo."

compile
